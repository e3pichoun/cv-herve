<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" integrity="sha512-oc9+XSs1H243/FRN9Rw62Fn8EtxjEYWHXRvjS43YtueEewbS6ObfXcJNyohjHqVKFPoXXUxwc+q1K7Dee6vv9g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css">
    <title>Hervé Fondeur</title>
    <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
</head>
<?php
if (empty($_POST)) :

?>

    <body>


        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <a class="navbar-brand" href="#ace">Hervé Fondeur</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#presentation">À propos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#competences">Compétences</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#projets">Projets</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#formation">Formations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#experience">Experience Professionnelle</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#formulaire">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a  class="nav-link js-scroll-trigger" href="images/CVHFR.pdf">CV</a>
                    </li>
                    <li class="nav-item">
                    <a  class="nav-link js-scroll-trigger" href="http://portefolio.herve-fondeur.labo-ve.fr/">Porte-Folio</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- ACCUEIL-->
        <section id="ace">
            <div class="jumbotron p-0 m-0">
                <video class="min-vh-100 min-vw-100" autoplay muted loop>
                    <source src="images/accueil.mp4" type="video/mp4">
                </video>
                <div class="card-img-overlay d-flex h-50 flex-column my-auto align-items-center justify-content-center text-center text-white">
                    <a href="#presentation" style="color:white; text-decoration:none"><h1 class="display-3">Hervé Fondeur</h1></a>
                    <a href="#presentation" style="color:white; text-decoration:none"><p class="lead">Développeur Web</p></a>
                </div>
            </div>
        </section>
        <!-- FIN ACCUEIL -->
        <!-- SECTION PRESENTATION -->
        <section id="presentation">
            <br>
            <h1 class="pt-md-5 text-center" id="about">À PROPOS</h1>
            <div class="container text-center">
                <div class="row">
                    <div class="col col-md-1"></div>
                    <div class="col-10 col-md-4 shadow presHerv" data-aos="fade-right">
                        <h2 class="pt-3">Développeur Web & Mobile</h2>
                        <img class="pt-1 mb-3" id="photoProfil" src="images/photoMoi.jpeg" alt="photo de profil" width="180em">
                    </div>
                    <div class="col col-md-2"></div>
                    <div class="col-10 col-md-4 shadow presHerv" data-aos="fade-left">
                        <p class="pt-5 pb-2">Passionné d'informatique, grâce à 20 ans d’exercices, j'aime découvrir de nouvelles
                            technologies, les assimiler et les utiliser à travers le développement, le déploiement et
                            l’aide à l’utilisation.</br></p>
                        <p class="pt-3"><strong>‘’ Un code pour tous, tous pour un code’’</strong></p>
                    </div>
                    <div class="col col-md-1"></div>
                </div>
            </div>
        </section>
        <!-- FIN SECTION PRESENTATION -->

        <!-- DEBUT SECTION COMPETENCES -->

        <section id="competences" data-aos="fade-up" data-aos-anchor-placement="center-bottom">
            <h1 class="pt-5 text-center">COMPETENCES</h1>
            <div class="container">
                <div class="row">
                    <div class="col col-md-1"></div>
                    <div class="col-10 col-md-4 shadow compHerv" data-aos="fade-right">
                        <img src="images/front.png" class="mx-auto d-block" alt="icon html css js">
                        <img src="images/back.png" class="mx-auto d-block" alt="icon php MySQL">
                        <img src="images/framework.png" class="mx-auto d-block" alt="icon Angular ReactJS Symfony">
                        <img src="images/other.png" class="mx-auto d-block" alt="icon bootstrap wordpress">
                    </div>
                    <div class="col"></div>
                    <div class="col col-md-2"></div>
                    <div class="col-10 col-md-4 shadow compHerv" data-aos="fade-left">
                        <h3 class="text-center">FRONT-END</h3>
                        <ul>
                            <li>Maquetter une application</li>
                            <li>Réaliser une interface utilisateur web statique et adaptable</li>
                            <li>Réaliser une interface utilisateur web dynamique</li>
                            <li>Réaliser une interface utilisateur avec une solution de gestion de contenu ou e-commerce
                            </li>
                        </ul>
                        <h3 class="text-center">BACK-END</h3>
                        <ul>
                            <li>Créer une base de données</li>
                            <li>Créer les composants d’accès aux données</li>
                            <li>Elaborer et mettre en œuvre des composants dans une application de gestion de contenu
                                ou e-commerce</li>
                        </ul>

                    </div>
                    <div class="col col-md-1"></div>
                </div>
            </div>
        </section>
        <!-- FIN SECTION COMPETENCES-->

        <!--DEBUT SECTION PROJETS -->
        <section id="projets" class="pb-5" data-aos="fade-up" data-aos-anchor-placement="center">
            <h1 class="pt-5 text-center">PROJETS</h1>
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-md-5 ">
                        <div class="col-12 col-md-12 text-center shadow projHerv" data-aos="zoom-in">
                            <p class="font-weight-bold">Site pour la SPA en landing page</p>
                            <a href="https://landingpagespa.vercel.app/">
                            <img src="images/projetsWeb02.png"alt="landing page spa"/></a>
                            <p class="font-weight-bold">Site de drum Star Wars</p>
                            <a href="https://drumstarwars.vercel.app/">
                            <img src="images/projetsWeb01.png" alt="drum Star Wars"/></a><br>
                            <a href="http://portefolio.herve-fondeur.labo-ve.fr/">
                            <img src="images/projetsWeb03.png" alt="portefolio"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- FIN SECTION PROJETS -->

        <!-- DEBUT SECTION FORMATION -->
        <section id="formation" class="pb-5" data-aos="fade-up" data-aos-anchor-placement="center-bottom">
            <div class="container">
                <h1 class="pt-5 text-center">FORMATIONS</h1>
                <div class="row">
                    <div class="col-md-7"></div>
                    <div class="col-10 col-md-4 text-center shadow formHerv" data-aos="zoom-in">
                        <h3 class="font-weight-bold">2021</h3>
                        <p class="font-weight-bold"> Titre Professionnel Développeur Web </p>
                        <p>Formation Simplon Le Cheylard -Bac +2</p>
                        <p>L'Ecole Numérique Ardéchoise - Le Cheylard 07160</p>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-10 col-md-4 text-center formHerv" data-aos="zoom-in">
                        <h3 class="font-weight-bold">2001 </h3>
                        <p class="font-weight-bold"> Bac Technologique STT ACTION COMMERCIALE</p>
                        <p>Lycée Gabriel Faure - Tournon-sur-Rhône 07300</p>
                    </div>
                    <div class="col-md-7"></div>
                </div>
            </div>
        </section>
        <!-- FIN SECTION FORMATION -->

        <!-- DEBUT SECTION EXPERIENCE -->
        <section id="experience" class="pb-5" data-aos="fade-up" data-aos-anchor-placement="center">
            <h1 class="pt-5 text-center">EXPERIENCE</h1>
            <div class="container text-center">
                <div class="row justify-content-start">
                    <div class="col-md-3 ">
                        <div class="col-12 col-md-12 text-center shadow expHerv" data-aos="fade-right">
                        <strong>Depuis 2018</strong><br>
                        <strong>Trésorier bénévole et Membre du CA</strong>
                        <p>Groupement Fnath Sud Est - 26000 Valence</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-md-3 ">
                        <div class="col-12 col-md-12 text-center shadow expHerv" data-aos="zoom-in">
                        <strong>2006 à 2018</strong><br>
                        <strong>Chef produit secteur et responsable développement</strong>
                        <p>EXPERT PONT TV - 38780 Pont-Evêque </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container text-center">
                <div class="row justify-content-end">
                    <div class="col-md-3 ">
                        <div class="col-12 col-md-12 text-center shadow expHerv" data-aos="fade-left">
                        <strong>2000 à 2017</strong><br>
                        <strong>Sapeur-pompier volontaire - Evolution de Sapeur à Sergent </strong>
                        <p>CIS Saint-Uze - 26240 et Ministère de l’intérieur Paris</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- FIN SECTION EXPERIENCE -->

        <!-- DEBUT SECTION FORMULAIRE -->

        <section id="formulaire" class="py-3">
            <h1 class="py-5 text-center">CONTACT</h1>
            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-10 col-md-5 text-center formuHerv">
                        <a href="http://www.linkedin.com/in/hervé-fondeur-175644193/" alt="linkedin">
                            <img src="images/linkedin.png" class="mb-5" /></a>
                        <form action="index.php" method="post" class="my-3">

                            <label id="nom" class="font-weight-bold"> Nom: </label></br>
                            <input class="champ" type="text" name="nom" minlength="2" required></br>

                            <label id="mail" class="font-weight-bold"> Mail: </label></br>
                            <input class="champ" type="email" name="email" minlength="7" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required></br>

                            <label for="msg" class="font-weight-bold">Message :</label></br>
                            <textarea id="msg" name="message" maxlength="400" required></textarea></br>
                            <input id="bouton" class="btn btn-dark mt-3" type="submit" value="Envoyer">
                        </form>

                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </section>
        <?php
    else :


        // 1 - Les données obligatoires sont-elles présentes ?
        if (isset($_POST["email"]) && isset($_POST["message"]) && isset($_POST["nom"])) {

            // 2 - Les données obligatoires sont-elles remplies ?
            if (!empty($_POST["email"]) && !empty($_POST["message"] && !empty($_POST["nom"]))) {

                // 3 - Les données obligatoires ont-elles une valeur conforme à la forme attendue ?
                if (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                    $_POST["email"] = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);

                    if (filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {
                        $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

                        if (filter_var($_POST["nom"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {


                            $to      = 'fondeur.rv@free.fr';
                            $subject = 'Contact Formulaire CV';
                            $message = $_POST['message'];
                            $headers = 'From:' . $_POST['email'] . PHP_EOL .
                                'Reply-To:' . $_POST['email'] . PHP_EOL .
                                'X-Mailer: PHP/' . phpversion();
                            mail($to, $subject, $message, $headers)
        ?>
                            <section id="formulaire" class="py-3">
                                <h1 class="pt-5 text-center">CONTACT</h1>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-10 col-md-5 text-center formuHerv">
                                            <a href="http://www.linkedin.com/in/hervé-fondeur-175644193/" alt="linkedin">
                                                <img src="images/linkedin.png" class="mb-5" /></a>
                                            <h3>Votre message a bien été envoyé</h3></br>
                                            <span>Merci</span></br>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                </div>
                            </section>

                            <meta http-equiv='Refresh' content='3;URL=index.php'>

                        <?php



                        } else {
                        ?>
                            <h1>'Merci de reformuler votre nom'</h1>
                            <meta http-equiv='Refresh' content='3;URL=index.php'>

                        <?php
                        }
                    } else {
                        ?>
                        <h1>'Merci de reformuler votre message'</h1>
                        <meta http-equiv='Refresh' content='3;URL=index.php'>

                    <?php
                    }
                } else {
                    ?>
                    <h1>"Votre adresse mail n'est pas valide"</h1>
                    <meta http-equiv='Refresh' content='3;URL=index.php'>

                <?php
                }
            } else {
                ?>
                <h1>"Merci de remplir correctement tous les champs"</h1>
                <meta http-equiv='Refresh' content='3;URL=index.php'>

            <?php
            }
        } else {
            ?>
            <h1>"Vos informations ne sont pas valides"</h1>
            <meta http-equiv='Refresh' content='3;URL=index.php'>

    <?php
        }


    endif;
    ?>


    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init({
            duration: 2000,
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.min.js" integrity="sha512-8qmis31OQi6hIRgvkht0s6mCOittjMa9GMqtK9hes5iEQBQE/Ca6yGE5FsW36vyipGoWQswBj/QBm2JR086Rkw==" crossorigin="anonymous"></script>


    </body>

</html>